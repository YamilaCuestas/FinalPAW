# Viajeros

### Idea Inicial del Proyecto

Se decidió desarrollar un sitio web de viajeros donde hay un/a administrador/a del sitio que puede publicar artículos
de tips para un viaje, recomendaciones, descripción de algún viaje en particular, etc.
Además el sitio permite a los usuarios que siguen este blog registrarse y poder hacer un diario de sus viajes,
y planificar el itinerario de los mismos.
La ultima funcionalidad que incluye el sitio es que los viajeros puedan proponer encuentros en distintas partes del
mundo con el objetivo de contactarse unos con otros.


### Decisiones Iniciales

* __Django__ es un framework web de python que esta estructurado básicamente en un proyecto principal formado por un conjunto de aplicaciones.
* Django responde al modelo Vista-Modelo-Template. Nuestro sitio respetara este modelo.
* Se comenzó definiendo un primer modelo de datos para el sitio.
* También se definieron las posibles apps que tendrá el proyecto.
* Se integrara un mapa en la pagina principal del sitio donde se podrá seleccionar puntos y obtener los artículos relacionados con ese punto.
* Evaluar el uso de: API de google Maps
* Sitio para tener de guía: https://viajandoporahi.com/

### Apps del proyecto:

* __Viajeros:__ Es el proyecto principal
  * `settings.py`: Configuraciones del proyecto.
  * `urls.py`:  Contiene los mapeos entre url solicitada y view
    -  ` ''` index
    - `/admin` admin --> a las views del admin provisto por Django
  * `views.py`: Contiene la funcionalidad, cargará lo que haga falta en algún template. Puede por lo tanto consultar los  modelos que sean necesarios.
    * index: Pagina principal del sitio de viajeros --> `/templates/index.html`

* __Blog:__ App encargada de administrar los distintos artículos. Solo podrán ser cargados por el administrador o administradora del sitio.
* __Usuario:__ Encargada de administrar el resto de los usuarios de mi sitio.
* __Diarios:__ Administra los diarios que son dados de alta por los usuarios de mi sitio
* __Viajes:__ Para administrar los distintos viajes que planifiquen los usuarios de mi sitio.
* __Eventos:__ Puntos de encuentro para reuniones durante los viajes.
### Proyecto Final de Programación en Ambiente Web



### Instalación del entorno de desarrollo

#### Linux Ubuntu

1. Crear un ambiente virtual para instalar las dependencias del proyecto (en nuestro caso utilizamos _virtualenv_ )


  ```
  ~> virtualenv -p python3 finalpaw
  Using base prefix '/usr'
  New python executable in finalpaw/bin/python3
  Also creating executable in finalpaw/bin/python
  Installing setuptools, pip...done.
  ```

2. Luego, dentro de la carpeta del entrono virtual clonar el proyecto.

  ```
  ~> cd finalpaw/
  ~/finalpaw> git clone https://yamilacuestas@gitlab.com/YamilaCuestas/FinalPAW.git
  Cloning into 'FinalPAW'...
  remote: Counting objects: 46, done.
  remote: Compressing objects: 100% (38/38), done.
  remote: Total 46 (delta 10), reused 0 (delta 0)
  Unpacking objects: 100% (46/46), done.
  Checking connectivity... done.
  ```

3. Activar el entorno virtual:

  ```
  ~/finalpaw> source bin/activate
  ```

4. Instalar las dependencias del archivo de requirements: __EL ENTORNO VIRTUAL DEBE ESTAR ACTIVADO__

  ```
  (virtualenv) pip3 install -r requirements.txt
  Downloading/unpacking Django==2.0.6 (from -r requirements.txt (line 1))
  Downloading Django-2.0.6-py3-none-any.whl (7.1MB): 7.1MB downloaded
  ..........
  Cleaning up...
  ```

5. Creacion de los modelos de base de datos


a. Correr las migraciones:

  ```
  ~/f/FinalPAW> python3 manage.py migrate
  Operations to perform:
    Apply all migrations: admin, auth, blog, contenttypes, sessions
  Running migrations:
    Applying contenttypes.0001_initial... OK
    Applying auth.0001_initial... OK
    Applying admin.0001_initial... OK
    Applying admin.0002_logentry_remove_auto_add... OK
    Applying contenttypes.0002_remove_content_type_name... OK
    Applying auth.0002_alter_permission_name_max_length... OK
    Applying auth.0003_alter_user_email_max_length... OK

  ```

6. Crear un súper usuario (Este paso es opcional):

Crear el super usuario permite ingresar al sitio de administracion del sitio de django y manipular a mano los modelos

  ```
  ~/f/FinalPAW> python3 manage.py createsuperuser
  Username (leave blank to use 'username'):
  Email address:
  Password:
  Password (again):
  Superuser created successfully.
  ```



7. Levantar el servidor local

  ```
  ~/f/FinalPAW> python3 manage.py runserver
  Performing system checks...

  System check identified no issues (0 silenced).
  June 18, 2018 - 13:23:59
  Django version 2.0.6, using settings 'viajeros.settings'
  Starting development server at http://127.0.0.1:8000/
  Quit the server with CONTROL-C.

  ```

8. Ejecutar el archivo seed.json para popular el Sitio con el siguiente comando.
    ```
      python manage.py loaddata db.json
    ```
    
Ingresar al navegador en la dirección y puerto indicado para acceder al sitio.



__El proyecto se encuentra listo para ser utilizado__
