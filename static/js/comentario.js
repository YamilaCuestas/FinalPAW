console.log("Loading comentario.js");

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

var csrftoken = getCookie('csrftoken');

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});


// Submit post on submit
$('#post-form').on('submit', function(event){
    event.preventDefault();
    console.log("form submitted!");  // sanity check
    crear_comentario();
});

function crear_comentario() {
    console.log("create post is working!"); // sanity check
    console.log($('#post-text').val());
    console.log($('#post-form').attr('action'));

    // make POST ajax call
    $.ajax({
        url: $('#post-form').attr('action'),
        type: 'POST',
        data: $('#post-text').val(),

        success: function (response) {
            $("#post-form").trigger('reset');
            console.log("Post request sent");
            $("#comentarios").load(" #comentarios");
        },

        error: function (response) {
            // alert the error if any error occured
            console.log("Post request could not be send");
        }

    });
};