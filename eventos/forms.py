from django.forms import ModelForm, Textarea, TextInput
from eventos.models import Evento, Participante


class NuevoEventoForm(ModelForm):
    class Meta:
        model = Evento
        fields = ('titulo', 'descripcion', 'lugar', 'ubicacion')
        widgets = {
            'titulo': TextInput(attrs={'size':'78'}),
            'descripcion': Textarea(attrs={'rows':'20', 'cols':'80'}),
            'lugar': TextInput(attrs={'size':'78'}),
            'ubicacion': TextInput(attrs={'size':'78'}),
        }


class ParticipanteForm(ModelForm):
    class Meta:
        model = Participante
        fields = ('evento', 'participa','alias')


class SearchEventoForm(ModelForm):
    class Meta:
        model = Evento
        fields = ('titulo',)
