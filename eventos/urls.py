
from django.conf.urls import url
from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.index, name='eventos'),
    path('nuevoEvento/', views.RegistrarEventoView.as_view(), name='nuevoEvento'),
    path('eventoVer/<pk>', views.eventoVer, name='eventoVer'),
    path('eventoBorrar/<pk>', views.eventoBorrar, name='eventoBorrar'),
    path('eventoEditar/<pk>', views.eventoEditar, name='eventoEditar'),
    path('eventoListar/', views.eventoListar, name='eventoListar'),
    path('verTodo/', views.VerEventosView.as_view(), name='verTodosEventos'),
    path('buscarEvento/', views.BuscarEventosView.as_view(), name='buscarEventos'),
    path('registrarParticipacion/<pk>/<user>', views.registrarParticipacion, name='registrarParticipacion')
]
