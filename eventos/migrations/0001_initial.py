# Generated by Django 2.1.5 on 2019-01-13 21:24

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Evento',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('titulo', models.CharField(blank=True, max_length=100, verbose_name='Titulo del evento')),
                ('descripcion', models.TextField(blank=True, max_length=500)),
                ('lugar', models.CharField(blank=True, max_length=100)),
                ('ubicacion', models.CharField(blank=True, max_length=100)),
                ('fecha', models.DateTimeField(blank=True, default=datetime.datetime.now)),
                ('cantidadParticipantes', models.IntegerField(blank=True, default=0)),
            ],
        ),
    ]
