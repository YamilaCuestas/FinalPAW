from django.contrib.auth.models import User
from django.db import models
from usuario.models import Perfil
from datetime import datetime


class Evento(models.Model):
    class Meta:
        verbose_name_plural = 'Eventos'

    usuario = models.ForeignKey(User, on_delete=models.CASCADE, default=None, blank=True, null=True)
    titulo = models.CharField("Titulo del evento", max_length=100, blank=True)
    descripcion = models.TextField(max_length=500, blank=True)
    lugar = models.CharField(max_length=100, blank=True)
    ubicacion = models.CharField(max_length=100, blank=True)
    fecha = models.DateTimeField(blank=True, default=datetime.now)
    cantidadParticipantes = models.IntegerField(blank=True, default=0)
    eliminado = models.BooleanField(default=False)
    fechaEntrada = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return self.titulo


class Participante(models.Model):
    evento = models.ForeignKey(Evento, on_delete=models.CASCADE, related_name='participantes')
    alias = models.CharField("Nombre o Alias para mostrar", max_length=30, blank=True)
    fecha = models.DateField(null=True, blank=True)
    participa = models.BooleanField(default=False)

    def __str__(self):
        return self.alias
