from django.contrib import admin
from eventos.models import Evento, Participante


admin.site.register(Evento)
admin.site.register(Participante)
