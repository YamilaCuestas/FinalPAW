import datetime

from django.core.paginator import Paginator
from django.shortcuts import render, redirect
from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic.base import View

from eventos.forms import NuevoEventoForm, SearchEventoForm, ParticipanteForm
from .models import Evento, Participante


def index(request):
    eventos = Evento.objects.all().filter(usuario=request.user.id).order_by('-fecha')
    paginator = Paginator(eventos, 3)
    page = request.GET.get('page')
    eventos = paginator.get_page(page)
    return render(request, 'eventos/eventos.html', {'items': eventos})

##################################################################################

class RegistrarEventoView(View):
    def get(self, request, *args, **kwargs):
        form = NuevoEventoForm()
        paramsDict = {'form': form}
        return render(request, 'eventos/nuevoEvento.html', paramsDict)

    def post(self, request, *args, **kwargs):
        form = NuevoEventoForm(request.POST, request.FILES)

        if form.is_valid():
            evento_form = form.save(commit=False)
            evento_form.fechaEntrada = datetime.datetime.now()
            form.save()

            return redirect('eventos')


##################################################################################



#VER
def eventoVer(request, pk):
    evento = Evento.objects.get(pk=pk)
    return render(request, 'eventos/verEvento.html', {'evento' : evento})

##################################################################################

#EDITAR
def eventoEditar(request, pk):
    evento = Evento.objects.get(pk=pk)
    if request.method == "POST":
        form = NuevoEventoForm(request.POST, instance=evento)
        if form.is_valid():
            evento = form.save(commit=False)
            evento.save()
            return render(request, 'eventos/verEvento.html', {'evento' : evento})
    else:
        form = NuevoEventoForm(instance=evento)
    return render(request, 'eventos/editarEvento.html', {'form': form})

##################################################################################

#BORRAR
def eventoBorrar(request, pk):
    evento = Evento.objects.get(pk=pk)
    evento.eliminado = True
    evento.save()

    #recargo la lista de articulos que no fueron eliminados
    #eventos = Evento.objects.exclude(eliminado__gte=True)
    #return render(request, 'eventos/eventos.html', {'eventos' : eventos})
    return redirect('eventos')
##################################################################################

#LISTAR
def eventoListar(request):
    eventos = Evento.objects.exclude(eliminado__gte=True).order_by('-fechaEntrada')
    return render(request, 'eventos/listarEventos.html', {'eventos' : eventos})

class VerEventosView(View):
    def get(self, request, *args, **kwargs):
        searchEventoForm = SearchEventoForm()
        eventos = Evento.objects.exclude(eliminado__gte=True).order_by('-fechaEntrada')
        paginator = Paginator(eventos, 3)
        page = request.GET.get('page')
        eventos = paginator.get_page(page)
        return render(request, 'eventos/verTodosLosEventos.html', {'searchForm': searchEventoForm,
                                                                  'eventos': eventos})

class BuscarEventosView(View):
    def get(self, request, *args, **kwargs):
        query = self.request.GET.get('q')
        if query == '':
            return redirect('verTodosEventos')
        else:
            eventos = Evento.objects.filter(titulo__icontains=query)
            paginator = Paginator(eventos, 3)
            page = request.GET.get('page')
            eventos = paginator.get_page(page)
            return render(request, 'eventos/verTodosLosEventos.html', {'eventos': eventos})


#registrarParticipacion
def registrarParticipacion(request, pk, user):
    evento = Evento.objects.get(pk=pk)
    if evento.cantidadParticipantes == 0:
        evento.cantidadParticipantes = evento.cantidadParticipantes + 1
        form = ParticipanteForm()
        participante = form.save(commit=False)
        participante.evento = evento
        participante.alias = user
        participante.participa = True
        participante.save()
        evento.save()
    else:
        participantes = Participante.objects.all().filter(evento=evento)

        lista_alias = []
        for participante in participantes:
            lista_alias.append(participante.alias)

        if user in lista_alias:
            participante = Participante.objects.get(evento=evento, alias=user)
            if participante.participa:
                evento.cantidadParticipantes = evento.cantidadParticipantes - 1
                participante.participa = False
            else:
                evento.cantidadParticipantes = evento.cantidadParticipantes + 1
                participante.participa = True
            participante.save()
            evento.save()
        else:
            evento.cantidadParticipantes = evento.cantidadParticipantes + 1
            form = ParticipanteForm()
            participante = form.save(commit=False)
            participante.evento = evento
            participante.alias = user
            participante.participa = True
            participante.save()
            evento.save()

    return render(request, 'eventos/verEvento.html', {'evento' : evento})
