import sys
sys.path.append(".")

from django import forms
from .models import Articulo, Comentario
from usuario.models import Perfil


class NuevoArticuloForm(forms.ModelForm):
    class Meta:
        model = Articulo
        fields = ('titulo', 'descripcion', 'contenido', 'pais', 'foto_articulo')
        widgets = {
            'titulo': forms.TextInput(attrs={'size':'78'}),
            'descripcion': forms.TextInput(attrs={'size':'78'}),
            'contenido': forms.Textarea(attrs={'rows':'20', 'cols':'80'}),
        }

class ComentarioForm(forms.ModelForm):

    class Meta:
        model = Comentario
        fields = ('texto',)

        widgets = {
            'texto': forms.Textarea(attrs={
                'rows':'20',
                'cols':'80',
                'id': 'post-text',
                'required': True,
                'placeholder': 'Que te parecio el articulo...'
            }),
        }


class SearchArticuloForm(forms.ModelForm):
    class Meta:
        model = Articulo
        fields = ('titulo',)


class SearchPerfilForm(forms.ModelForm):
    class Meta:
        model = Perfil
        fields = ('alias',)
