import datetime

from django.contrib.auth.models import User
from django.core import serializers
from django.http import JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from .forms import NuevoArticuloForm, SearchArticuloForm, SearchPerfilForm, ComentarioForm
from .models import Articulo
from django.core.paginator import Paginator
from django.views.generic.base import View

from viaje.models import Diario, Pagina


def index(request):
    articulos = Articulo.objects.exclude(eliminado__gte="True").order_by('-fechaEntrada')
    paginator = Paginator(articulos, 3)
    page = request.GET.get('page')
    articulos = paginator.get_page(page)
    return render(request, 'blog/articulos.html', {'items': articulos})


class RegistrarArticuloView(View):
    def get(self, request, *args, **kwargs):
        form = NuevoArticuloForm()
        paramsDict = {'form': form}
        return render(request, 'blog/crear-articulo.html', paramsDict)

    def post(self, request, *args, **kwargs):
        form = NuevoArticuloForm(request.POST, request.FILES)

        if form.is_valid():
            articulo_form = form.save(commit=False)
            articulo_form.fechaEntrada = datetime.datetime.now()
            form.save()

            return redirect('articulos')


def verArticulo(request, pk):
    comentarioForm = ComentarioForm()
    articulo = Articulo.objects.get(pk=pk)
    articulo.foto_articulo.name = articulo.foto_articulo.name.split('static/')[1]
    return render(request, 'blog/verArticulo.html', {'articulo': articulo,
                                                     'comentarioForm': comentarioForm})


def editarArticulo(request, pk):
    articulo = Articulo.objects.get(pk=pk)
    if request.method == "POST":
        form = NuevoArticuloForm(request.POST, instance=articulo)
        if form.is_valid():
            articulo = form.save(commit=False)
            articulo.save()
            return render(request, 'blog/verArticulo.html', {'articulo' : articulo})
    else:
        form = NuevoArticuloForm(instance=articulo)
    return render(request, 'blog/crear-articulo.html', {'form': form})


def borrarArticulo(request, pk):
    articulo = Articulo.objects.get(pk=pk)
    articulo.delete()

    return redirect('articulos')


def listarArticulos(request):
    articulos = Articulo.objects.exclude(eliminado__gte=True).order_by('-fechaEntrada')
    paginator = Paginator(articulos, 3)
    page = request.GET.get('page')
    articulos = paginator.get_page(page)
    return render(request, 'blog/listarArticulos.html', {'items': articulos})


class VerArticulosView(View):
    def get(self, request, *args, **kwargs):
        searchArticuloForm = SearchArticuloForm()
        articulos = Articulo.objects.exclude(eliminado__gte=True).order_by('-fechaEntrada')
        paginator = Paginator(articulos, 3)
        page = request.GET.get('page')
        articulos = paginator.get_page(page)
        return render(request, 'blog/verTodosLosArticulos.html', {'searchForm': searchArticuloForm,
                                                                  'articulos': articulos})


class VerDiariosView(View):
    def get(self, request, *args, **kwargs):
        searchPerfilForm = SearchPerfilForm()
        diarios = Diario.objects.all()
        paginator = Paginator(diarios, 3)
        page = request.GET.get('page')
        diarios = paginator.get_page(page)
        return render(request, 'blog/verTodosLosDiarios.html', {'searchForm': searchPerfilForm,
                                                                'diarios': diarios})


class BuscarArticuloView(View):
    def get(self, request, *args, **kwargs):
        query = self.request.GET.get('q')
        articulos = Articulo.objects.filter(titulo__icontains=query)
        paginator = Paginator(articulos, 3)
        page = request.GET.get('page')
        articulos = paginator.get_page(page)
        return render(request, 'blog/verTodosLosArticulos.html', {'articulos': articulos})


class BuscarDiarioView(View):
    def get(self, request, *args, **kwargs):
        query = self.request.GET.get('q')
        usuarios = User.objects.filter(username__icontains=query)
        diarios = Diario.objects.filter(usuario__in=usuarios)
        paginator = Paginator(diarios, 3)
        page = request.GET.get('page')
        diarios = paginator.get_page(page)
        return render(request, 'blog/verTodosLosDiarios.html', {'diarios': diarios})


class VisualizarDiarioUsuario(View):
    def get(self, request, pk, *args, **kwargs):
        diario = Diario.objects.get(pk=pk)
        paginas = Pagina.objects.all().filter(diario=diario.id).order_by('-fechaEntrada')

        for pagina in paginas:
            pagina.foto.name = pagina.foto.name.split('static/')[1]
            print (pagina)

        forms = {
            'paginas': paginas
        }

        return render(request, 'diario/visualizarDiario.html', forms)
        if query == '':
            return redirect('verTodosArticulos')
        else:
            articulos = Articulo.objects.filter(titulo__icontains=query)
            paginator = Paginator(articulos, 3)
            page = request.GET.get('page')
            articulos = paginator.get_page(page)
            return render(request, 'blog/verTodosLosArticulos.html', {'articulos': articulos})



class AgregarComentarioView(View):
    def get(self, request, *args, **kwargs):
        form = ComentarioForm()
        return render(request, 'blog/agregarComentario.html', {'form': form})

    def post(self, request, pk, *args, **kwargs):
        if self.request.is_ajax:
            articulo = get_object_or_404(Articulo, pk=pk)
            form = ComentarioForm(request.POST)
            if form.is_valid():
                comentario = form.save(commit=False)
                comentario.articulo = articulo
                comentario.autor = request.user.username
                comentario.fechaCreacion = datetime.datetime.now()
                comentario.texto = request.body.decode("utf-8")
                comentario.save()

                comentario_ser = serializers.serialize('json', [comentario, ])

                return JsonResponse({'comentario': comentario_ser}, status=200)

        JsonResponse({"error": "error al guardar el comentario"}, status=400)
