import datetime

from django.db import models


class Pais(models.Model):
    nombre = models.CharField("Nombre del pais", max_length=100)
    descripcion = models.CharField("Descripción del pais", max_length=300, blank=True, null=True)

    def __str__(self):
        return self.nombre


class Articulo(models.Model):

    class Meta:
        verbose_name_plural = 'Articulos'

    titulo = models.CharField("Titulo del articulo", max_length=100)
    descripcion = models.CharField("Descripción", max_length=300, blank=True, null=True)
    contenido = models.TextField("Contenido")
    pais = models.ForeignKey(Pais, on_delete=models.CASCADE)
    foto_articulo = models.ImageField("Foto del Artículo",  blank=True, upload_to='static/img/articulos/')
    me_gusta = models.IntegerField(default=0)
    eliminado = models.BooleanField(default=False)
    fechaEntrada = models.DateField(null=True, blank=True)

    def __str__(self):
        return self.titulo


class Comentario(models.Model):
    articulo = models.ForeignKey(Articulo, on_delete=models.CASCADE, related_name='comentarios', null=True, blank=True)
    autor = models.CharField(max_length=200, null=True, blank=True)
    texto = models.TextField(null=True, blank=True)
    fechaCreacion = models.DateField(null=True, blank=True)

    def __str__(self):
        return self.texto
