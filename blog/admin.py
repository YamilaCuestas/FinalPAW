from django.contrib import admin

from .models import Articulo, Pais, Comentario

admin.site.register(Articulo)
admin.site.register(Comentario)
admin.site.register(Pais)
