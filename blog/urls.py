from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='articulos'),
    path('alta/', views.RegistrarArticuloView.as_view(), name='articuloAlta'),
    path('articulo/<pk>', views.verArticulo, name='articuloVer'),
    path('editar/<pk>', views.editarArticulo, name='articuloEditar'),
    path('baja/<pk>', views.borrarArticulo, name='articuloBorrar'),
    path('listar/', views.listarArticulos, name='articulosListar'),
    path('verTodo/', views.VerArticulosView.as_view(), name='verTodosArticulos'),
    path('buscarArticulo/', views.BuscarArticuloView.as_view(), name='buscarArticulos'),
    path('buscarDiario/', views.BuscarDiarioView.as_view(), name='buscarDiarios'),
    path('verTodoDiarios/', views.VerDiariosView.as_view(), name='verTodosDiarios'),
    path('visualizarDiarioUsuario/<pk>', views.VisualizarDiarioUsuario.as_view(), name='visualizarDiarioUsuario'),
    path('articulo/<pk>/comentario/', views.AgregarComentarioView.as_view(), name='agregarComentario')
]
