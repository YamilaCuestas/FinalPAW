from django.db import models
from django.contrib.auth.models import User


class Perfil(models.Model):
    usuario = models.OneToOneField(User, on_delete=models.CASCADE)
    alias = models.CharField("Nombre o Alias para mostrar", max_length=30, null=True, blank=True)
    bio = models.TextField("Biografia", max_length=500, null=True, blank=True)
    lugar = models.CharField(max_length=30, null=True, blank=True)
    nacimiento = models.DateField(null=True, blank=True)
    foto_perfil = models.ImageField("Foto de Perfil", null=True, blank=True,
                                    default='static/img/user_icon.jpg', upload_to='static/img/perfiles/')

    def __str__(self):
        return self.alias
