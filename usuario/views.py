from django.shortcuts import render, redirect
from django.http import HttpResponse

from django.contrib.auth import login, authenticate

from .forms import SignUpForm, PerfilForm
from .models import Perfil


def registrar(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('inicio')
    else:
        form = SignUpForm()
    return render(request, 'usuario/registrar.html', {'form': form,})


def registrar_perfil(request):
    if request.method == 'POST':
        # Cargo los dos formularios para cuando se registra tenga que completar los
        # datos de Perfil
        form = SignUpForm(request.POST, request.FILES)
        form2= PerfilForm(request.POST, request.FILES)
        if form.is_valid() and form2.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            perfil = form2.save(commit=False)
            perfil.usuario = request.user
            perfil.save()
            return redirect('panel')
    else:
        form = SignUpForm()
        form2 = PerfilForm()
    return render(request, 'usuario/registrar.html', {'form': form, 'form2':form2})