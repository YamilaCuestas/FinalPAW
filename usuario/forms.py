from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

from .models import Perfil

class SignUpForm(UserCreationForm):
    first_name = forms.CharField(label='Nombre',max_length=30, required=False, help_text='Opcional')
    last_name = forms.CharField(label='Apellido', max_length=30, required=False, help_text='Opcional')
    email = forms.EmailField(label='Email', max_length=254, help_text='Requiere una dirección de correo valida')

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2', )


class PerfilForm(forms.ModelForm):

    # alias = forms.CharField(label="Nombre o Alias para mostrar", max_length=30)
    # bio = forms.CharField(widget=forms.Textarea,label="Biografia", max_length=500)
    # lugar = forms.CharField(label="¿Donde Naciste?", max_length=30)
    # nacimiento = forms.DateField(widget=forms.DateInput, label="Fecha de nacimiento")

    class Meta:
        model = Perfil
        fields = ('alias', 'bio', 'lugar', 'nacimiento', 'foto_perfil')
        widgets = {
            'alias': forms.TextInput(attrs={'size':'80'}),
            'bio': forms.Textarea(attrs={'rows':'10', 'cols':'80'}),
            'lugar': forms.TextInput(attrs={'size':'80'}),
            'nacimiento': forms.TextInput(attrs={'size':'80'}),
        }
