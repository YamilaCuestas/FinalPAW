from django.urls import path, re_path, include
from django.conf import settings
from . import views

from django.contrib.auth import views as auth_views

urlpatterns = [
    path('registrar/', views.registrar_perfil, name='registrar'),
    # Utilizamos el login provisto por Django pero con un template propio
    path('ingresar/', auth_views.LoginView.as_view(), {'template_name': 'registration/login.html'}, name='ingresar'),
    path('salir/', auth_views.LogoutView.as_view(), {'next_page': settings.LOGOUT_REDIRECT_URL}, name='salir'),
]
