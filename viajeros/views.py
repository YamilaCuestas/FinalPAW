from django.core.files import File
from django.shortcuts import render, redirect
from blog.models import Articulo
from django.views.generic.base import View
from eventos.models import Evento
from usuario.forms import PerfilForm
from usuario.models import Perfil
from viaje.models import Viaje, Diario


class IndexView(View):
    def get(self, request, *args, **kwargs):
        articulos = Articulo.objects.exclude(eliminado__gte="True").order_by('-fechaEntrada')[:3]
        for articulo in articulos:
            articulo.foto_articulo.name = articulo.foto_articulo.name.split('static/')[1]

        eventos = Evento.objects.exclude(eliminado__gte="True").order_by('-fechaEntrada')[:3]
        viajes = Viaje.objects.all().order_by('-fechaEntrada')[:3]
        diarios = Diario.objects.all()[:3]

        for diario in diarios:
            diario.usuario.perfil.foto_perfil.name = diario.usuario.perfil.foto_perfil.name.split('static/')[1]

        params_dict = {'articulos': articulos, 'eventos': eventos, 'viajes': viajes, 'diarios': diarios}

        return render(request, 'index.html', params_dict)


class PanelView(View):
    def get(self, request, *args, **kwargs):
        try:
            perfil = request.user.perfil
        except Exception:
            perfil = Perfil()
            perfil.usuario = request.user
            perfil.alias = request.user.username
            perfil.save()
        finally:
            perfil.foto_perfil.name = perfil.foto_perfil.name.split('static/')[1]
            form = PerfilForm(instance=perfil)

            return render(request, 'panel.html', {'form': form, 'perfil_info': perfil})

    def post(self, request, *args, **kwargs):
        form = PerfilForm(request.POST, request.FILES)

        if form.is_valid():
            perfil = Perfil.objects.get(usuario=request.user)

            perfil.alias = form.cleaned_data.get('alias')
            perfil.bio = form.cleaned_data.get('bio')
            perfil.lugar = form.cleaned_data.get('lugar')
            perfil.nacimiento = form.cleaned_data.get('nacimiento')
            perfil.foto_perfil = form.cleaned_data.get('foto_perfil')

            perfil.save()

            return redirect('panel')
