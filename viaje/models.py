from django.contrib.auth.models import User
from django.db import models
from usuario.models import Perfil


class Recorrido(models.Model):
    nombreViaje = models.CharField("Nombre o Alias para mostrar", max_length=30, blank=True)
    lugar = models.CharField(max_length=30, blank=True)
    destino = models.TextField(max_length=500, blank=True)
    desde = models.DateField(null=True, blank=True)
    hasta = models.DateField(null=True, blank=True)

    def __str__(self):
        return self.nombreViaje


class Viaje(models.Model):
    usuario = models.ForeignKey(User, on_delete=models.CASCADE, default=None, blank=True, null=True)
    nombre = models.CharField("Nombre del viaje", max_length=50, blank=True)
    descripcion = models.CharField("Describi tu viaje", max_length=500, blank=True)
    objetivos = models.CharField("Cuales son tus expectativas, ideales, u objetivos a lograr", max_length=500, blank=True)
    fechaEntrada = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return self.nombre


class Destino(models.Model):
    pais = models.CharField("Pais de destino", max_length=50, blank=True)
    ciudad = models.CharField("Ciudad de destino", max_length=50, blank=True)
    cantDias = models.IntegerField("Cantidad de dias aproximados que te vas a quedar", blank=True)
    fechaLlegada = models.DateField(null=True, blank=True)
    fechaSalida = models.DateField(null=True, blank=True)
    viaje = models.ForeignKey(Viaje, on_delete=models.CASCADE, default=None, blank=True, null=True)

    def __str__(self):
        return self.pais


class Diario(models.Model):
    usuario = models.OneToOneField(User, on_delete=models.CASCADE)
    nombre = models.CharField("Nombre del diario", max_length=50, blank=True)
    fechaCreacion = models.DateField(null=True, blank=True)

    def __str__(self):
        return self.nombre


class Pagina(models.Model):
    titulo = models.CharField("Titulo de la entrada", max_length=50, blank=True)
    fechaEntrada = models.DateField(null=True, blank=True)
    encabezado = models.CharField("Breve descripción", max_length=300, blank=True)
    cuerpo = models.CharField("Escribí acá tus experiencias", max_length=500, blank=True)
    foto = models.ImageField("Foto de la entrada",  blank=True, upload_to='static/img/paginas/')
    diario = models.ForeignKey(Diario, on_delete=models.CASCADE, default=None, blank=True, null=True)

    def __str__(self):
        return self.titulo


class Viajero(models.Model):
    usuario = models.OneToOneField(Perfil, on_delete=models.CASCADE, default=None)
    ubicacionEnMapa = models.CharField("En que lugar del mapa estas?", max_length=50, blank=True)
    viajes = models.ForeignKey(Viaje, on_delete=models.CASCADE)
    diarios = models.ForeignKey(Diario, on_delete=models.CASCADE)
