from django.contrib import admin
from .models import Destino, Viaje, Viajero, Diario, Pagina

admin.site.register(Destino)
admin.site.register(Viaje)
admin.site.register(Viajero)
admin.site.register(Diario)
admin.site.register(Pagina)
