from django.contrib import admin
from django.conf.urls import url
from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.index, name='viaje'),
    path('nuevoViaje/', views.NuevoViajeView.as_view(), name='nuevoViaje'),
    path('editarViaje/<pk>', views.EditarViajeView.as_view(), name='editarViaje'),
    path('verViaje/<pk>', views.VerViajeView.as_view(), name='verViaje'),
    path('borrar_viaje/<pk>', views.borrar_viaje, name='borrar_viaje'),
    path('verDiario/', views.VerDiarioView.as_view(), name='diario'),
    path('nuevaPagina/', views.NuevaPaginaView.as_view(), name='nuevaPagina'),
    path('editarPagina/<pk>', views.EditarPaginaView.as_view(), name='editarPagina'),
    path('visualizarDiario/', views.VisualizarDiarioView.as_view(), name='visualizarDiario'),
    path('borrar_pagina/<pk>', views.borrar_pagina, name='borrar_pagina'),
    path('verTodo/', views.VerViajesView.as_view(), name='verTodosViajes'),
    path('buscarViaje/', views.BuscarViajesView.as_view(), name='buscarViajes')
]
