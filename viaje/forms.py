from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
#from .models import Post
from django.forms.models import inlineformset_factory
from django.forms.models import BaseInlineFormSet


from . import models
from .models import Recorrido, Viaje, Destino, Pagina


class SignUpForm(forms.ModelForm):
    lugar = forms.CharField(label='Lugar',max_length=50, required=False, help_text='Opcional')
    desde = forms.DateField(label='Desde', required=False, help_text='Opcional')
    hasta = forms.DateField(label='Hasta', required=False, help_text='Opcional')

    class Meta:
        model = Recorrido
        fields = ('lugar', 'desde', 'hasta',)

class SearchViajeForm(forms.ModelForm):
    class Meta:
        model = Viaje
        fields = ('nombre',)

class CreateViajeForm(forms.ModelForm):

    class Meta:
        model = Viaje
        fields = ('nombre', 'descripcion', 'objetivos',)
        widgets = {
            'nombre': forms.TextInput(attrs={'size':'78'}),
            'descripcion': forms.Textarea(attrs={'rows':'10', 'cols':'80'}),
            'objetivos': forms.Textarea(attrs={'rows':'10', 'cols':'80'}),
        }


class EditarDestinoForm(forms.ModelForm):

    # pais = forms.CharField(label="Pais de destino", max_length=50, required=True,
    #                        widget=forms.TextInput(attrs={'placeholder': 'Pais'}))
    # ciudad = forms.CharField(label="Ciudad de destino", max_length=50, required=False,
    #                          widget=forms.TextInput(attrs={'placeholder': 'Ciudad'}))
    # cantDias = forms.IntegerField(label="Cantidad de dias aproximados que te vas a quedar", required=False,
    #                               widget=forms.TextInput(attrs={'placeholder': 'Nro de Dias'}))
    # fechaLlegada = forms.DateField(label='Fecha de llegada', required=False,
    #                                widget=forms.TextInput(attrs={'placeholder': 'Fecha de llegada'}))
    # fechaSalida = forms.DateField(label='Fecha de salida', required=False,
    #                               widget=forms.TextInput(attrs={'placeholder': 'Fecha de salida'}))

    class Meta:
        model = Destino
        fields = ('pais', 'ciudad', 'cantDias', 'fechaLlegada', 'fechaSalida')
        widgets = {
            'pais': forms.TextInput(attrs={'size':'78'}),
            'ciudad': forms.TextInput(attrs={'size':'78'}),
            'cantDias': forms.TextInput(attrs={'size':'78'}),
            'fechaLlegada': forms.DateInput(attrs={'size':'78'}),
            'fechaSalida': forms.DateInput(attrs={'size':'78'}),
        }

class EditarViajeForm(forms.ModelForm):
    # nombre = forms.CharField(label='Nombre',max_length=50, required=True, help_text='Opcional',
    #                          widget=forms.TextInput(attrs={'placeholder': 'nombre'}))
    # descripcion = forms.CharField(label='Descripcion',max_length=100, required=False, help_text='Opcional',
    #                               widget=forms.TextInput(attrs={'placeholder': 'descripcion'}))
    # objetivos = forms.CharField(label='Objetivos del viaje',max_length=100, required=False, help_text='Opcional',
    #                             widget=forms.TextInput(attrs={'placeholder': 'objetivos'}))

    class Meta:
        model = Viaje
        fields = ('nombre', 'descripcion', 'objetivos',)
        widgets = {
            'nombre': forms.TextInput(attrs={'size':'78'}),
            'descripcion': forms.Textarea(attrs={'rows':'10', 'cols':'80'}),
            'objetivos': forms.Textarea(attrs={'rows':'10', 'cols':'80'}),
        }

class CreatePaginaForm(forms.ModelForm):
    # titulo = forms.CharField(label='Nombre',max_length=50, required=True, help_text='Opcional',
    #                          widget=forms.TextInput(attrs={'placeholder': 'titulo de la entrada'}))
    # encabezado = forms.CharField(label='Encabezado',max_length=100, required=False, help_text='Opcional',
    #                              widget=forms.TextInput(attrs={'placeholder': 'encabezado'}))
    # cuerpo = forms.CharField(label='Cuerpo',max_length=50, required=True, help_text='Opcional',
    #                          widget=forms.TextInput(attrs={'placeholder': 'Entrada'}))
    foto = forms.ImageField()

    class Meta:
        model = Pagina
        fields = ('titulo', 'encabezado', 'cuerpo', 'foto')
        widgets = {
            'titulo': forms.TextInput(attrs={'size':'78'}),
            'encabezado': forms.Textarea(attrs={'rows':'10', 'cols':'80'}),
            'cuerpo': forms.Textarea(attrs={'rows':'10', 'cols':'80'}),
        }
