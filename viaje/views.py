import datetime
from django.forms import modelformset_factory
from django.shortcuts import render, redirect
from django.core.paginator import Paginator
from django.views.generic.base import View

from .forms import CreateViajeForm, EditarViajeForm, EditarDestinoForm, CreatePaginaForm, SearchViajeForm
from .models import Viaje, Destino, Diario, Pagina


def index(request):
    viajes = Viaje.objects.all().filter(usuario=request.user.id).order_by('-fechaEntrada')
    paginator = Paginator(viajes, 3)
    page = request.GET.get('page')
    viajes = paginator.get_page(page)
    return render(request, 'viaje/index.html', {'items': viajes})


class NuevoViajeView(View):
    def get(self, request, *args, **kwargs):
        form = CreateViajeForm()
        return render(request, 'viaje/nuevoViaje.html', {'form': form})

    def post(self, request, *args, **kwargs):
        form = CreateViajeForm(request.POST)
        if form.is_valid():
            viaje_form = form.save(commit=False)
            viaje_form.usuario = request.user
            viaje_form.fechaEntrada = datetime.datetime.now()
            form.save()
            return redirect('viaje')


class VerViajesView(View):
    def get(self, request, *args, **kwargs):
        searchViajeForm = SearchViajeForm()
        viajes = Viaje.objects.all().order_by('-fechaEntrada')
        paginator = Paginator(viajes, 3)
        page = request.GET.get('page')
        viajes = paginator.get_page(page)
        return render(request, 'viaje/verTodosLosViajes.html', {'searchForm': searchViajeForm,
                                                                  'viajes': viajes})


class BuscarViajesView(View):
    def get(self, request, *args, **kwargs):
        query = self.request.GET.get('q')
        if query == '':
            return redirect('verTodosViajes')
        else:
            viajes = Viaje.objects.filter(nombre__icontains=query)
            paginator = Paginator(viajes, 3)
            page = request.GET.get('page')
            viajes = paginator.get_page(page)
            return render(request, 'viaje/verTodosLosViajes.html', {'viajes': viajes})


class VerViajeView(View):
    def get(self, request, pk, *args, **kwargs):
        viaje = Viaje.objects.get(id=pk)
        destinos = Destino.objects.all().filter(viaje=pk)

        forms = {
            'viaje': viaje,
            'destinos': destinos
        }

        return render(request, 'viaje/verViaje.html', forms)


class EditarViajeView(View):
    def get(self, request, pk, *args, **kwargs):
        viaje = Viaje.objects.get(id=pk)
        editar_viaje_form = EditarViajeForm(instance=viaje, prefix='viaje')

        destino = Destino.objects.all().filter(viaje=pk)
        destino_formset = modelformset_factory(Destino, form=EditarDestinoForm, can_delete=True, extra=1, max_num=None,
                                               validate_max=False, min_num=None, validate_min=False)
        editar_destino_formset = destino_formset(queryset=destino, prefix='destinos')

        forms = {
            'editar_viaje_form': editar_viaje_form,
            'editar_destino_formset': editar_destino_formset
        }

        return render(request, 'viaje/editarViaje.html', forms)

    def post(self, request, pk, *args, **kwargs):
        viaje = Viaje.objects.get(id=pk)
        viaje_form = EditarViajeForm(request.POST, instance=viaje, prefix='viaje')

        destino = modelformset_factory(Destino, form=EditarDestinoForm, can_delete=True,  max_num=None,
                                       validate_max=False, min_num=None, validate_min=False)
        destino_formset = destino(request.POST, prefix='destinos')

        if viaje_form.is_valid():
            viaje_form.save(commit=True)

        if destino_formset.is_valid():
            instances = destino_formset.save(commit=False)
            for instance in instances:
                instance.viaje = viaje
                instance.save()
            destino_formset.save(commit=True)

        viajes = Viaje.objects.all().filter(usuario=request.user.id)
        paginator = Paginator(viajes, 3)
        page = request.GET.get('page')
        viajes = paginator.get_page(page)
        return render(request, 'viaje/index.html', {'items': viajes})


def borrar_viaje(request, pk):
    viaje = Viaje.objects.get(pk=pk)
    viaje.delete()

    return redirect('viaje')


class VerDiarioView(View):
    def get(self, request, *args, **kwargs):
        try:
            diario = Diario.objects.get(usuario=request.user.id)
            paginas = Pagina.objects.all().filter(diario=diario.id)
            if len(paginas) != 0:
                return render(request, 'diario/index.html', {'paginas': paginas})
            else:
                return render(request, 'diario/index.html', {'paginas': None})
        except Exception:
            diario = Diario(usuario=request.user,
                            nombre='Diario_' + request.user.username,
                            fechaCreacion=datetime.datetime.now())
            diario.usuario = request.user
            diario.save()
            return render(request, 'diario/index.html', {'paginas': None})


class NuevaPaginaView(View):
    def get(self, request, *args, **kwargs):
        form = CreatePaginaForm()
        return render(request, 'diario/nuevaPagina.html', {'form': form})

    def post(self, request, *args, **kwargs):
        form = CreatePaginaForm(request.POST, request.FILES)
        if form.is_valid():
            pagina_form = form.save(commit=False)
            pagina_form.diario = Diario.objects.get(usuario=request.user)
            pagina_form.fechaEntrada = datetime.datetime.now()
            form.save()
        return redirect('diario')


class EditarPaginaView(View):
    def get(self, request, pk, *args, **kwargs):
        pagina = Pagina.objects.get(id=pk)
        editar_pagina_form = CreatePaginaForm(instance=pagina, prefix='pagina')

        forms = {
            'pagina_form': editar_pagina_form,
        }

        return render(request, 'diario/editarPagina.html', forms)

    def post(self, request, pk, *args, **kwargs):
        pagina = Pagina.objects.get(pk=pk)
        form = CreatePaginaForm(request.POST, request.FILES, instance=pagina, prefix='pagina')
        if form.is_valid():
            form.save()
        return redirect('diario')


class VisualizarDiarioView(View):
    def get(self, request, *args, **kwargs):
        diario = Diario.objects.get(usuario=request.user.id)
        paginas = Pagina.objects.all().filter(diario=diario.id).order_by('-fechaEntrada')

        for pagina in paginas:
            pagina.foto.name = pagina.foto.name.split('static/')[1]
            print (pagina)

        forms = {
            'paginas': paginas
        }

        return render(request, 'diario/visualizarDiario.html', forms)


def borrar_pagina(request, pk):
    pagina = Pagina.objects.get(pk=pk)
    pagina.delete()

    return redirect('diario')
